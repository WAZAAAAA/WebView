﻿using System;

namespace WebView.Example
{
    internal static class Program
    {
        private static WebView s_webView;

        [STAThread]
        private static void Main()
        {
            Console.WriteLine("Creating webview...");

            s_webView = new WebViewBuilder()
                .Title("Test 123")
                .Resizable()
                .Size(800, 600)
                .Uri("https://google.com")
                .ExternalCallback(ExternalCallback)
                .Build();

            s_webView.Run();
        }

        private static void ExternalCallback(WebView webView, string arg)
        {
            Console.WriteLine(arg);
        }
    }
}
