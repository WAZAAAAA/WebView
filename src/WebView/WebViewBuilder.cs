﻿using System;

namespace WebView
{
    public class WebViewBuilder
    {
        private Uri _uri;
        private string _title;
        private (int width, int height) _size;
        private bool _resizable;
        private bool _debug;
        private Action<WebView, string> _externalCallback;

        public WebViewBuilder Uri(Uri uri)
        {
            _uri = uri;
            return this;
        }

        public WebViewBuilder Uri(string uri)
        {
            return Uri(new Uri(uri));
        }

        public WebViewBuilder Title(string title)
        {
            _title = title;
            return this;
        }

        public WebViewBuilder Size(int width, int height)
        {
            _size = (width, height);
            return this;
        }

        public WebViewBuilder Resizable()
        {
            _resizable = true;
            return this;
        }

        public WebViewBuilder Debug()
        {
            _debug = true;
            return this;
        }

        public WebViewBuilder ExternalCallback(Action<WebView, string> callback)
        {
            _externalCallback = callback;
            return this;
        }

        public WebView Build()
        {
            return new WebView(_title, _uri.ToString(), _size, _resizable, _debug, _externalCallback);
        }
    }
}
