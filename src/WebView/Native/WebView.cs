﻿using System.Runtime.InteropServices;

namespace WebView.Native
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct WebView
    {
        [MarshalAs(UnmanagedType.LPStr)]
        public string Url;

        [MarshalAs(UnmanagedType.LPStr)]
        public string Title;

        public int Width;
        public int Height;
        public int Resizable;
        public int Debug;

        [MarshalAs(UnmanagedType.FunctionPtr)]
        public WebViewExternalInvokeCallback ExternalInvoke;

        public fixed byte priv_data[60];
    }
}
