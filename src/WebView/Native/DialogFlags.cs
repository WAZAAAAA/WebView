﻿using System;

namespace WebView.Native
{
    [Flags]
    public enum DialogFlags
    {
        File = 0 << 0,
        Directory = 1 << 0,
        Info = 1 << 1,
        Warning = 2 << 1,
        Error = 3 << 1,
        AlertMask = 3 << 1
    }
}
